FROM python:3.6-slim-stretch

LABEL maintainer="daniel.miles@pwc.com"

COPY requirements.txt requirements.txt

RUN  pip install -r requirements.txt
